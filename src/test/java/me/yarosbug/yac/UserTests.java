package me.yarosbug.yac;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import me.yarosbug.yac.model.Room;
import me.yarosbug.yac.model.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import static io.restassured.RestAssured.given;
import static java.util.Arrays.asList;
import static me.yarosbug.yac.util.Util.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;

@SuppressWarnings("ArraysAsListWithZeroOrOneArgument")
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserTests {

    @LocalServerPort
    int port;

    @Before
    public void setUp() {
        RestAssured.port = port;
        RestAssured.requestSpecification = new RequestSpecBuilder()
                .setContentType(ContentType.JSON)
                .setAccept(ContentType.JSON)
                .build();
        resetChat();
    }

    @Test
    public void createUser_userCreated() {
        User user = given().body("{\"name\": \"bob2\"}")
                .when()
                .post("/users")
                .then()
                .statusCode(200)
                .body("name", equalTo("bob2"))
                .body("id", notNullValue())
                .extract().as(User.class);

        given().when()
                .get("/users/{id}", user.getId())
                .then()
                .statusCode(200)
                .body("name", equalTo("bob2"))
                .body("id", equalTo(user.getId()));
    }

    @Test
    public void createUser_nameIsEmpty_badRequest() {
        given().body("{\"name\": \"\"}")
                .when()
                .post("/users")
                .then()
                .statusCode(HttpStatus.BAD_REQUEST.value());
    }

    @Test
    public void deleteUser_userNotJoinedAnyRooms_userDeleted() {
        User user = createUser("bob");

        given().when()
                .delete("/users/{id}", user.getId())
                .then()
                .statusCode(200);

        given().when()
                .get("/users/{id}", user.getId())
                .then()
                .statusCode(HttpStatus.NOT_FOUND.value());
    }

    @Test
    public void deleteUser_userJoinedRoom_userDeletedAndRelatedMessagedDeleted() {
        User user1 = createUser("bob1");
        User user2 = createUser("bob2");
        Room room = createRoom("room1");
        joinRoom(user1.getId(), room.getId());
        joinRoom(user2.getId(), room.getId());
        sendMessage("1-1", user1.getId(), room.getId());
        sendMessage("2-1", user2.getId(), room.getId());

        given().when()
                .delete("/users/{id}", user1.getId())
                .then()
                .statusCode(HttpStatus.OK.value());

        given().when()
                .get("/users/{id}", user1.getId())
                .then()
                .statusCode(HttpStatus.NOT_FOUND.value());

        assertThat(getMessages(asList(user1.getId()), null, null)).isEmpty();
    }
}
