package me.yarosbug.yac;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import me.yarosbug.yac.model.Room;
import me.yarosbug.yac.model.User;
import me.yarosbug.yac.util.BasicSetup;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static io.restassured.RestAssured.*;
import static java.util.Arrays.asList;
import static me.yarosbug.yac.util.Util.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;

@SuppressWarnings("ArraysAsListWithZeroOrOneArgument")
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RoomTests {

    @LocalServerPort
    int port;

    @Before
    public void setUp() {
        RestAssured.port = port;
        RestAssured.requestSpecification = new RequestSpecBuilder()
                .setContentType(ContentType.JSON)
                .setAccept(ContentType.JSON)
                .build();
        resetChat();
    }

    @Test
    public void createRoom_roomCreated() {
        Room room = given().body("{\"name\": \"room1\"}")
                .when()
                .post("/rooms")
                .then()
                .statusCode(200)
                .body("name", equalTo("room1"))
                .body("id", notNullValue())
                .extract().as(Room.class);

        given().when()
                .get("/rooms/{id}", room.getId())
                .then()
                .statusCode(200)
                .body("name", equalTo("room1"))
                .body("id", equalTo(room.getId()));
    }

    @Test
    public void createRoom_nameIsEmpty_badRequest() {
        given().body("{\"name\": \"\"}")
                .when()
                .post("/rooms")
                .then()
                .statusCode(HttpStatus.BAD_REQUEST.value());
    }

    @Test
    public void getAllRooms_chatHasRooms_allRoomsReturned() {
        BasicSetup bs = new BasicSetup();

        assertThat(getAllRooms().stream().map(Room::getId))
                .containsOnly(bs.room1.getId(), bs.room2.getId());
    }

    @Test
    public void deleteRoom_roomDoesNotExit_notFound() {
        given().when()
                .delete("/rooms/{id}", "NON_EXISTING_ROOM_ID")
                .then()
                .statusCode(HttpStatus.NOT_FOUND.value());
    }

    @Test
    public void deleteRoom_roomExists_notFoundWhenRetrievingById() {
        Room room1 = createRoom("room1");

        delete("/rooms/{id}", room1.getId()).then().statusCode(HttpStatus.OK.value());

        given().when().get("/rooms/{id}", room1.getId()).then()
                .statusCode(HttpStatus.NOT_FOUND.value());
    }

    @Test
    public void deleteRoom_roomExists_relatedMassagesDeleted() {
        BasicSetup bs = new BasicSetup();

        deleteRoom(bs.room1.getId());

        assertThat(getMessages(null, asList(bs.room1.getId()), null)).isEmpty();
    }

    @Test
    public void deleteRoom_roomExists_relatedUsersLeftTheRoom() {
        BasicSetup bs = new BasicSetup();
        List<User> usersInRoom1 = getRoomUsers(bs.room1.getId());

        deleteRoom(bs.room1.getId());

        for (User user : usersInRoom1) {
            assertThat(getUserRooms(user.getId()).stream().map(Room::getId)).doesNotContain(bs.room1.getId());
        }
    }

    @Test
    public void joinRoom_userDoesNotExists_userNotFound() {
        Room room = createRoom("room");

        joinRoom("NON_EXISTING_USER_ID", room.getId())
                .then().statusCode(HttpStatus.NOT_FOUND.value());
    }

    @Test
    public void joinRoom_roomDoesNotExists_userNotFound() {
        User user = createUser("bob");

        joinRoom(user.getId(), "NON_EXISTING_ROOM_ID")
                .then().statusCode(HttpStatus.NOT_FOUND.value());
    }

    @Test
    public void joinRoom_noRoomsJoined_joined() {
        User user = createUser("bob");
        Room room = createRoom("room");

        joinRoom(user.getId(), room.getId());

        List<Room> userRooms = getUserRooms(user.getId());
        assertThat(userRooms).hasSize(1);
        assertThat(userRooms.get(0).getId()).isEqualTo(room.getId());
    }

    @Test
    public void joinRoom_alreadyInThisRoom_error() {
        User user = createUser("bob");
        Room room = createRoom("room");
        joinRoom(user.getId(), room.getId());

        joinRoom(user.getId(), room.getId())
                .then()
                .statusCode(HttpStatus.CONFLICT.value());
    }

    @Test
    public void leaveRoom_roomDoesNotExist_notFound() {
        User user = createUser("bob");

        given().when()
                .post("/users/{userId}/room/{roomId}/leave", user.getId(), "NON_EXISTING_ROOM")
                .then()
                .statusCode(HttpStatus.NOT_FOUND.value());
    }

    @Test
    public void leaveRoom_userNotInThisRoom_notFound() {
        User user = createUser("bob");
        Room room = createRoom("room1");

        given().when()
                .post("/users/{userId}/room/{roomId}/leave", user.getId(), room.getId())
                .then()
                .statusCode(HttpStatus.NOT_FOUND.value());
    }

    @Test
    public void leaveRoom_userIsInThisRoom_userHasLeftTheRoom() {
        User user = createUser("bob");
        Room room = createRoom("room1");
        joinRoom(user.getId(), room.getId());

        leaveRoom(user.getId(), room.getId());

        assertThat(getUserRooms(user.getId())).isEmpty();
        assertThat(getRoomUsers(room.getId()).stream().map(User::getId)).doesNotContain(user.getId());
    }
}
