package me.yarosbug.yac;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;
import me.yarosbug.yac.model.Message;
import me.yarosbug.yac.model.Room;
import me.yarosbug.yac.model.User;
import me.yarosbug.yac.util.BasicSetup;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static io.restassured.RestAssured.given;
import static java.util.Arrays.asList;
import static me.yarosbug.yac.util.Util.*;
import static org.assertj.core.api.Assertions.assertThat;

@SuppressWarnings("ArraysAsListWithZeroOrOneArgument")
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MessageTests {

    @LocalServerPort
    private int port;

    @Before
    public void setUp() {
        RestAssured.port = port;
        RestAssured.requestSpecification = new RequestSpecBuilder()
                .setContentType(ContentType.JSON)
                .setAccept(ContentType.JSON)
                .build();
        resetChat();
    }

    @Test
    public void getMessages_noFilters_getAllMessages() {
        BasicSetup bs = new BasicSetup();

        List<Message> messages = getAllMessages();

        assertThat(messages).hasSize(4);
    }

    @Test
    public void getMessages_filterByRoom_getAllMessagesInThatRoom() {
        BasicSetup bs = new BasicSetup();

        List<Message> messages = getMessages(null, asList(bs.room1.getId()), null);

        assertThat(messages.stream().map(Message::getId))
                .containsOnly(bs.msg11.getId(), bs.msg21.getId());
    }

    @Test
    public void getMessages_filterBySender_getAllMessagesOfThatSender() {
        BasicSetup bs = new BasicSetup();

        List<Message> messages = getMessages(asList(bs.user1_12.getId()), null, null);

        assertThat(messages.stream().map(Message::getId))
                .containsOnly(bs.msg11.getId(), bs.msg12.getId());
    }

    @Test
    public void getMessages_filterByRoomAndSender_getFilteredMessages() {
        BasicSetup bs = new BasicSetup();

        List<Message> messages = getMessages(asList(bs.user1_12.getId()), asList(bs.room1.getId()), null);

        assertThat(messages.stream().map(Message::getId))
                .containsOnly(bs.msg11.getId());
    }

    @Test
    public void getMessages_filterByRoomAndDate_getFilteredMessages() {
        BasicSetup bs = new BasicSetup();

        List<Message> messages = getMessages(null, asList(bs.room1.getId()), bs.msg21.getSentAt());

        assertThat(messages.stream().map(Message::getId))
                .containsOnly(bs.msg21.getId());
    }

    @Test
    public void getMessages_filterBySenderAndDate_getFilteredMessages() {
        BasicSetup bs = new BasicSetup();

        List<Message> messages = getMessages(asList(bs.user1_12.getId()), null, bs.msg12.getSentAt());

        assertThat(messages.stream().map(Message::getId))
                .containsOnly(bs.msg12.getId());
    }

    @Test
    public void getMessages_filterByRoomAndSenderAndAfterDate_getFilteredMessages() {
        BasicSetup bs = new BasicSetup();
        Message lateMsg = sendMessage("1-1-late", bs.user1_12.getId(), bs.room1.getId());
        sendMessage("2-1-late", bs.user2_1.getId(), bs.room1.getId());

        List<Message> messages = getMessages(asList(bs.user1_12.getId()), asList(bs.room1.getId()), lateMsg.getSentAt());

        assertThat(messages.stream().map(Message::getId))
                .containsOnly(lateMsg.getId());
    }

    @Test
    public void getMessages_filterByAfterDate_onlyMessagesAfterDateReturned() {
        BasicSetup bs = new BasicSetup();

        List<Message> messages = getMessages(null, null, bs.msg12.getSentAt());

        assertThat(messages.stream().map(Message::getId))
                .containsOnly(bs.msg12.getId(), bs.msg21.getId(), bs.msg32.getId());
    }

    @Test
    public void sendMessage_userIsInRoom_messageSent() {
        User user1 = createUser("bob1");
        Room room1 = createRoom("room1");
        joinRoom(user1.getId(), room1.getId());

        Message msg11 = sendMessage("1-1", user1.getId(), room1.getId());

        Message sentMsg = getMessage(msg11.getId());
        assertThat(sentMsg.getUserId()).isEqualTo(user1.getId());
        assertThat(sentMsg.getRoomId()).isEqualTo(room1.getId());
        assertThat(sentMsg.getContent()).isEqualTo("1-1");
    }

    @Test
    public void sendMessage_userIsNotInRoom_notFound() {
        User user1 = createUser("bob1");
        Room room1 = createRoom("room1");

        sendMessageHelloAndValidate(user1.getId(), room1.getId())
                .statusCode(HttpStatus.NOT_FOUND.value());
    }

    @Test
    public void sendMessage_roomDoesNotExist_notFound() {
        User user1 = createUser("bob1");

        sendMessageHelloAndValidate(user1.getId(), "NON_EXISTING_ROOM_ID")
                .statusCode(HttpStatus.NOT_FOUND.value());
    }

    @Test
    public void sendMessage_userDoesNotExist_notFound() {
        Room room1 = createRoom("room1");

        sendMessageHelloAndValidate("NON_EXISTING_USER_ID", room1.getId())
                .statusCode(HttpStatus.NOT_FOUND.value());
    }

    @Test
    public void sendMessage_userAndRoomDoNotExist_notFound() {
        sendMessageHelloAndValidate("NON_EXISTING_USER_ID", "NON_EXISTING_ROOM_ID")
                .statusCode(HttpStatus.NOT_FOUND.value());
    }

    @Test
    public void sendMessage_emptyMessage_badRequest() {
        User user1 = createUser("bob1");
        Room room1 = createRoom("room1");
        joinRoom(user1.getId(), room1.getId());

        given().body("{\"content\": \"\"}")
                .post("/users/{userId}/rooms/{roomId}/messages", user1.getId(), room1.getId())
                .then()
                .statusCode(HttpStatus.BAD_REQUEST.value());
    }

    private static ValidatableResponse sendMessageHelloAndValidate(String userId, String roomId) {
        return given().body("{\"content\": \"Hello!\"}")
                .when()
                .post("/users/{userId}/rooms/{roomId}/messages", userId, roomId)
                .then();
    }

}
