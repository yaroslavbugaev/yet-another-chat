package me.yarosbug.yac.util;

import me.yarosbug.yac.model.Message;
import me.yarosbug.yac.model.Room;
import me.yarosbug.yac.model.User;

import static me.yarosbug.yac.util.Util.*;

public class BasicSetup {

    public User user1_12;
    public User user2_1;
    public User user3_2;
    public Room room1;
    public Room room2;
    public Message msg11;
    public Message msg12;
    public Message msg21;
    public Message msg32;

    public BasicSetup() {
        user1_12 = createUser("bob1");
        user2_1 = createUser("bob2");
        user3_2 = createUser("bob3");
        room1 = createRoom("room1");
        room2 = createRoom("room2");
        joinRoom(user1_12.getId(), room1.getId());
        joinRoom(user1_12.getId(), room2.getId());
        joinRoom(user2_1.getId(), room1.getId());
        joinRoom(user3_2.getId(), room2.getId());
        msg11 = sendMessage("1-1", user1_12.getId(), room1.getId());
        msg12 = sendMessage("1-2", user1_12.getId(), room2.getId());
        msg21 = sendMessage("2-1", user2_1.getId(), room1.getId());
        msg32 = sendMessage("2-1", user3_2.getId(), room2.getId());
    }
}
