package me.yarosbug.yac.util;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import me.yarosbug.yac.model.Message;
import me.yarosbug.yac.model.Room;
import me.yarosbug.yac.model.User;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static io.restassured.RestAssured.*;

public class Util {
    private static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");

    public static Room createRoom(String name) {
        return given().body(String.format("{\"name\": \"%s\"}", name))
                .post("/rooms").then().extract().as(Room.class);
    }

    public static User createUser(String name) {
        return given().body(String.format("{\"name\": \"%s\"}", name))
                .post("/users").then().extract().as(User.class);
    }

    public static Response joinRoom(String userId, String roomId) {
        return post("/users/{userId}/rooms/{roomId}/join", userId, roomId);
    }

    public static void leaveRoom(String userId, String roomId) {
        delete("/users/{userId}/rooms/{roomId}/leave", userId, roomId);
    }

    public static List<Room> getUserRooms(String userId) {
        return get("/users/{userId}/rooms", userId).jsonPath().getList(".", Room.class);
    }

    public static List<User> getRoomUsers(String roomId) {
        return get("/rooms/{userId}/users", roomId).jsonPath().getList(".", User.class);
    }

    public static List<Message> getAllMessages() {
        return get("/messages").jsonPath().getList(".", Message.class);
    }

    public static List<Message> getMessages(List<String> userIds, List<String> roomIds, Date after) {
        RequestSpecification req = given();
        if (userIds != null) req.param("senders", userIds);
        if (roomIds != null) req.param("rooms", roomIds);
        if (after != null) req.param("after", SDF.format(after));

        return req.get("/messages").jsonPath().getList(".", Message.class);
    }

    public static Message sendMessage(String content, String userId, String roomId) {
        return given().body(String.format("{\"content\": \"%s\"}", content))
                .post("/users/{userId}/rooms/{roomId}/messages", userId, roomId)
                .as(Message.class);
    }

    public static Message getMessage(String msgId) {
        return get("/messages/{id}", msgId).as(Message.class);
    }

    public static List<Room> getAllRooms() {
        return get("/rooms").jsonPath().getList(".", Room.class);
    }

    public static void deleteRoom(String roomId) {
        delete("/rooms/{id}", roomId);
    }

    public static void resetChat() {
        get("/reset");
    }
}
