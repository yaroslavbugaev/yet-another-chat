package me.yarosbug.yac.services;

import me.yarosbug.yac.model.Message;
import me.yarosbug.yac.model.Room;
import me.yarosbug.yac.model.User;

import java.util.Date;
import java.util.List;

public interface ChatService {
    void reset();
    //-----------------
    //    Messages
    //-----------------

    Message getMessage(String msgId);

    List<Message> getMessages(List<String> senders, List<String> rooms, Date after);

    Message sendMessage(String text, String fromUser, String toRoom);

    void deleteMessage(String msgId);
    //-----------------
    //    Users
    //-----------------
    User getUser(String userId);

    List<User> getUsers();

    User createUser(String username);

    void deleteUser(String userId);

    List<Room> getUserRooms(String userId);

    //-----------------
    //    Rooms
    //-----------------
    Room createRoom(String name);

    void joinRoom(String userId, String roomId);

    void leaveRoom(String userId, String roomId);

    void deleteRoom(String roomId);

    Room getRoom(String roomId);

    List<Room> getRooms();

    List<User> getRoomUsers(String roomId);
}
