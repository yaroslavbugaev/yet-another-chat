package me.yarosbug.yac.services;

import lombok.AllArgsConstructor;
import me.yarosbug.yac.exceptions.AlreadyExistsException;
import me.yarosbug.yac.exceptions.ResourceNotFoundException;
import me.yarosbug.yac.model.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@Service
public class ChatServiceImpl implements ChatService {

    private final UserRepository userRepo;
    private final RoomRepository roomRepo;
    private final UserRoomRepository userRoomRepo;
    private final MessageRepository msgRepo;

    @Override
    public void reset() {
        userRepo.deleteAll();
        roomRepo.deleteAll();
        userRoomRepo.deleteAll();
        msgRepo.deleteAll();
    }

    @Override
    public Message getMessage(String msgId) {
        return msgRepo.findById(msgId).orElse(null);
    }

    @Override
    public List<Message> getMessages(List<String> senders, List<String> rooms, Date after) {
        if (senders == null) senders = Collections.emptyList();
        if (rooms == null) rooms = Collections.emptyList();
        if (after == null) after = new Date(0);

        return msgRepo.findAll(senders, rooms, after);
    }

    @Transactional
    @Override
    public Message sendMessage(String text, String fromUser, String toRoom) {
        userRoomRepo.findByUserIdAndRoomId(fromUser, toRoom)
                .orElseThrow(() -> new ResourceNotFoundException("User x Room mapping not found"));

        Message msg = new Message(text, fromUser, toRoom);
        return msgRepo.save(msg);
    }

    @Override
    public void deleteMessage(String msgId) {
        msgRepo.deleteById(msgId);
    }

    @Override
    public User getUser(String userId) {
        return userRepo.findById(userId).orElse(null);
    }

    @Override
    public List<User> getUsers() {
        return userRepo.findAll();
    }

    @Override
    public User createUser(String username) {
        return userRepo.save(new User(username));
    }

    @Transactional
    @Override
    public void deleteUser(String userId) {
        userRepo.findById(userId).orElseThrow(() -> new ResourceNotFoundException("User not found"));
        userRepo.deleteById(userId);
        userRoomRepo.deleteAllByUserId(userId);
        msgRepo.deleteByUserId(userId);
    }

    @Transactional
    @Override
    public List<Room> getUserRooms(String userId) {
        List<String> roomIds = userRoomRepo.findAllByUserId(userId).stream().map(UserRoom::getRoomId).collect(Collectors.toList());
        return roomRepo.findAllByIdIsIn(roomIds);
    }

    @Override
    public Room createRoom(String name) {
        return roomRepo.save(new Room(name));
    }

    @Transactional
    @Override
    public void joinRoom(String userId, String roomId) {
        if(!userRepo.existsById(userId)) {
            throw new ResourceNotFoundException("User not found");
        }

        if(!roomRepo.existsById(roomId)) {
            throw new ResourceNotFoundException("Room not found");
        }

        userRoomRepo.findByUserIdAndRoomId(userId, roomId).ifPresent((ur) -> {
            throw new AlreadyExistsException("User already in the room");
        });

        userRoomRepo.save(new UserRoom(userId, roomId));
    }

    @Transactional
    @Override
    public void leaveRoom(String userId, String roomId) {
        UserRoom userRoom = userRoomRepo.findByUserIdAndRoomId(userId, roomId)
                .orElseThrow(() -> new ResourceNotFoundException("User is not in the room"));

        userRoomRepo.deleteById(userRoom.getId());
    }

    @Transactional
    @Override
    public void deleteRoom(String roomId) {
        roomRepo.findById(roomId).orElseThrow(() -> new ResourceNotFoundException("Room not found"));
        roomRepo.deleteById(roomId);
        userRoomRepo.deleteAllByRoomId(roomId);
        msgRepo.deleteByRoomId(roomId);
    }

    @Override
    public Room getRoom(String roomId) {
        return roomRepo.findById(roomId).orElse(null);
    }

    @Override
    public List<Room> getRooms() {
        return roomRepo.findAll();
    }

    @Transactional
    @Override
    public List<User> getRoomUsers(String roomId) {
        List<String> userIds = userRoomRepo.findAllByRoomId(roomId).stream().map(UserRoom::getUserId).collect(Collectors.toList());
        return userRepo.findAllByIdIsIn(userIds);
    }
}
