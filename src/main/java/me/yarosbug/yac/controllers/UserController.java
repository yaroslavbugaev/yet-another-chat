package me.yarosbug.yac.controllers;

import lombok.AllArgsConstructor;
import me.yarosbug.yac.exceptions.ResourceNotFoundException;
import me.yarosbug.yac.model.Message;
import me.yarosbug.yac.model.Room;
import me.yarosbug.yac.model.User;
import me.yarosbug.yac.services.ChatService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@AllArgsConstructor
@RestController
public class UserController {

    private final ChatService chatService;

    @GetMapping("/users")
    List<User> all() {
        return chatService.getUsers();
    }

    @GetMapping("/users/{id}")
    User one(@PathVariable String id) {
        User user = chatService.getUser(id);
        if (user == null) {
            throw new ResourceNotFoundException("User not found");
        }

        return user;
    }

    @PostMapping("/users")
    User newUser(@Validated @RequestBody User newUser) {
        return chatService.createUser(newUser.getName());
    }

    @DeleteMapping("/users/{userId}")
    void deleteUser(@PathVariable String userId) {
        chatService.deleteUser(userId);
    }

    @PostMapping(value = "/users/{userId}/rooms/{roomId}/messages")
    Message sendMessage(@Validated @RequestBody Message newMsg,
                        @PathVariable("userId") String userId,
                        @PathVariable("roomId") String roomId) {
        return chatService.sendMessage(newMsg.getContent(), userId, roomId);
    }

    @PostMapping(value = "/users/{userId}/rooms/{roomId}/join")
    void joinRoom(@PathVariable("userId") String userId,
                  @PathVariable("roomId") String roomId) {
        chatService.joinRoom(userId, roomId);
    }

    @DeleteMapping(value = "/users/{userId}/rooms/{roomId}/leave")
    void leaveRoom(@PathVariable("userId") String userId,
                   @PathVariable("roomId") String roomId) {
        chatService.leaveRoom(userId, roomId);
    }

    @GetMapping(value = "/users/{userId}/rooms")
    List<Room> getUserJoinedRooms(@PathVariable String userId) {
        return chatService.getUserRooms(userId);
    }
}
