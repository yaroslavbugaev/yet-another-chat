package me.yarosbug.yac.controllers;

import lombok.AllArgsConstructor;
import me.yarosbug.yac.model.Room;
import me.yarosbug.yac.model.User;
import me.yarosbug.yac.services.ChatService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
public class DevController {

    private final ChatService chatService;

    @GetMapping(value = "/reset")
    void reset() {
        chatService.reset();
    }

    @GetMapping(value = "/")
    String home() {
        return "Welcome to YAC! Docs: https://documenter.getpostman.com/view/2941221/S1ENxeBu";
    }

    @GetMapping(value = "/preset")
    void preset() {
        chatService.reset();

        User bob1_12 = chatService.createUser("bob1_12");
        User bob2_1 = chatService.createUser("bob2_1");
        User bob3_2 = chatService.createUser("bob3_2");

        Room room1 = chatService.createRoom("room1");
        Room room2 = chatService.createRoom("room2");

        chatService.joinRoom(bob1_12.getId(), room1.getId());
        chatService.joinRoom(bob1_12.getId(), room2.getId());
        chatService.joinRoom(bob2_1.getId(), room1.getId());
        chatService.joinRoom(bob3_2.getId(), room2.getId());

        chatService.sendMessage("Hello from bob1_12 to Room#1", bob1_12.getId(), room1.getId());
        chatService.sendMessage("Hello from bob1_12 to Room#2", bob1_12.getId(), room2.getId());
        chatService.sendMessage("Hello from bob2_1 to Room#1", bob2_1.getId(), room1.getId());
        chatService.sendMessage("Hello from bob3_2 to Room#2", bob3_2.getId(), room2.getId());
    }
}
