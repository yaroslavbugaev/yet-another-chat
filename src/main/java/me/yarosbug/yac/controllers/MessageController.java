package me.yarosbug.yac.controllers;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import me.yarosbug.yac.exceptions.ResourceNotFoundException;
import me.yarosbug.yac.model.Message;
import me.yarosbug.yac.services.ChatService;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@Slf4j
@AllArgsConstructor
@RestController
public class MessageController {
    private final ChatService chatService;

    @GetMapping("/messages")
    List<Message> all(@RequestParam(value = "senders", required = false) List<String> userIds,
                      @RequestParam(value = "rooms", required = false) List<String> roomIds,
                      @RequestParam(value = "after", required = false)
                      @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) Date afterDate) {
        return chatService.getMessages(userIds, roomIds, afterDate);
    }

    @GetMapping("/messages/{id}")
    Message one(@PathVariable String id) {
        Message msg = chatService.getMessage(id);
        if (msg == null) {
            throw new ResourceNotFoundException("Message not found");
        }

        return msg;
    }

    @DeleteMapping("/messages/{id}")
    void deleteMessage(@PathVariable String id) {
        chatService.deleteMessage(id);
    }
}
