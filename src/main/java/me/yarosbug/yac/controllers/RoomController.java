package me.yarosbug.yac.controllers;

import lombok.AllArgsConstructor;
import me.yarosbug.yac.exceptions.ResourceNotFoundException;
import me.yarosbug.yac.model.Room;
import me.yarosbug.yac.model.User;
import me.yarosbug.yac.services.ChatService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@AllArgsConstructor
@RestController
public class RoomController {
    private final ChatService chatService;

    @GetMapping("/rooms")
    List<Room> all() {
        return chatService.getRooms();
    }

    @GetMapping("/rooms/{id}")
    Room one(@PathVariable String id) {
        Room room = chatService.getRoom(id);
        if (room == null) {
            throw new ResourceNotFoundException("Room not found");
        }

        return room;
    }

    @PostMapping("/rooms")
    Room newRoom(@Validated @RequestBody Room newRoom) {
        return chatService.createRoom(newRoom.getName());
    }

    @DeleteMapping("/rooms/{id}")
    void deleteRoom(@PathVariable String id) {
        chatService.deleteRoom(id);
    }

    @GetMapping("/rooms/{id}/users")
    List<User> getUsersInRoom(@PathVariable String id) {
        return chatService.getRoomUsers(id);
    }
}
