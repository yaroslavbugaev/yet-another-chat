package me.yarosbug.yac.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotEmpty;

@Data
@RequiredArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Document
public class Room {
    @Id
    private String id;
    @NotEmpty
    @NonNull
    private String name;
}
