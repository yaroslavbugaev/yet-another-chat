package me.yarosbug.yac.model;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRoomRepository extends MongoRepository<UserRoom, String> {
    Optional<UserRoom> findByUserIdAndRoomId(String userId, String roomId);
    List<UserRoom> findAllByRoomId(String roomId);
    List<UserRoom> findAllByUserId(String userId);
    void deleteAllByUserId(String userId);
    void deleteAllByRoomId(String roomId);
}
