package me.yarosbug.yac.model;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotEmpty;
import java.util.Date;

@Data
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Document
public class Message {
    @Id
    private String id;
    @NotEmpty
    private String content;
    private Date sentAt = new Date();

    private String userId;
    private String roomId;

    public Message(@NotEmpty String content, String userId, String roomId) {
        this.content = content;
        this.userId = userId;
        this.roomId = roomId;
    }
}
