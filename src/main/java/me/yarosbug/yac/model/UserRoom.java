package me.yarosbug.yac.model;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Document(collection = "users_rooms")
public class UserRoom {
    @Id
    private String id;
    private String userId;
    private String roomId;

    public UserRoom(String userId, String roomId) {
        this.userId = userId;
        this.roomId = roomId;
    }
}
