package me.yarosbug.yac.model;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface MessageRepository extends MongoRepository<Message, String> {

    @Query(value = "{$and :["
            + "{ $or : [ { $where: '?0.length == 0' } , { 'userId' : { $in : ?0 } } ] },"
            + "{ $or : [ { $where: '?1.length == 0' } , { 'roomId' : { $in : ?1 } } ] },"
            + "{ 'sentAt' : { $gte : ?2 } }"
            + "]}", sort = "{sentAt : 1}")
    List<Message> findAll(List<String> userIds, List<String> roomIds, Date after);

    void deleteByRoomId(String roomId);

    void deleteByUserId(String userId);
}
